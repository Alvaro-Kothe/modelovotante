import random
from typing import Counter

import ModeloVotante
from ModeloVotante.influenciador import InfluenciadorRede


def gera_sistema_2() -> ModeloVotante.ModeloVotante:
    atores = [
        ModeloVotante.Ator(i + 1, 1 if (i + 1) % 2 == 0 else -1) for i in range(10)
    ]
    grupos = {1: range(5), 2: range(5, 10)}

    comunidades = (grupos[1 if i < 5 else 2] for i in range(10))
    influenciadores = (InfluenciadorRede() for _ in range(10))

    modelo = ModeloVotante.ModeloVotante(atores)
    modelo.set_influenciador_por_ator(influenciadores, comunidades)

    return modelo


def ex_2_2():
    modelo = gera_sistema_2()

    return modelo.simular_modelo(200)


def ex_2_5():
    vetor_1_eq_6 = []
    for _ in range(100):
        modelo = gera_sistema_2()

        modelo.simular_modelo(200)
        ator_origem = modelo.get_origem(modelo.lista_atores[0], 200)

        vetor_1_eq_6.append(
            ator_origem == modelo.get_origem(modelo.lista_atores[5], 200)
        )

    return sum(vetor_1_eq_6) / len(vetor_1_eq_6)


if __name__ == "__main__":
    random.seed(1)
    print(ex_2_2())

    random.seed(1)
    # Ex 2.3
    counter = Counter(str(ex_2_2()) for _ in range(30))
    for k, v in counter.most_common(4):
        print(k, v, "Ocorrências")

    random.seed(1)
    print(ex_2_5())
