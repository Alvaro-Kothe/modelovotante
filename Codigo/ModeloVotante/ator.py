from __future__ import annotations
from typing import Any, Sequence, Tuple
import random


class Ator:
    """Representa o ator no modelo votante"""

    def __init__(self, nome, opiniao=-1, mutavel: bool = True) -> None:
        self.nome = nome
        self._opiniao = opiniao
        self._mutavel = mutavel
        self.log: dict[int, Tuple[Ator, Any]] = {0: (self, opiniao)}
        self.influenciadores: Sequence[Ator] = None

    def get_opiniao(self) -> Any:
        return self._opiniao

    def set_opiniao(self, tempo, influenciador, opiniao):
        if self._mutavel:
            self._opiniao = opiniao
            self.log.update({tempo: (influenciador, opiniao)})

    def set_influenciadores(self, influenciadores: Sequence[Ator]) -> None:
        self.influenciadores = influenciadores

    def sorteia_influenciador(self) -> Ator:
        return random.choice(tuple(self.influenciadores))

    def __repr__(self) -> str:
        return f"{self.nome}"
