import random
from typing import Iterable, Sequence, Tuple, Any
from .ator import Ator
from .influenciador import Influenciador


class ModeloVotante:
    """Modelo Votante"""

    def __init__(
        self,
        lista_atores: Sequence[Ator],
        influenciador: Influenciador = None,
    ) -> None:
        self.lista_atores = lista_atores
        self.influenciador = influenciador
        self.tempo = 1

        self.log: dict[int, Tuple[Ator, Ator]] = dict()  # (tempo: (A_t, I_t))

        if self.influenciador is not None:
            for ator in self.lista_atores:
                # Caso seja a mesma forma de escolha de influenciador
                # para todos os atores.
                influenciadores = self.influenciador.listar_influenciadores(
                    ator, lista_atores
                )
                ator.set_influenciadores(influenciadores)

    def set_influenciador_por_ator(
        self,
        lista_inluenciador: Iterable[Influenciador],
        lista_indices_atores: Sequence[Sequence[int]] | None = None,
    ) -> None:
        """Configura os influenciadores por ator

        Args:
            lista_inluenciador (Sequence[Influenciador]): Lista de classes a
            serem utilizadas por ator
            lista_indices_atores (list[int] | None, optional): Lista de indíces
            de python se referindo aos grupos em que os atores estão inseridos. Defaults to None.
        """
        if lista_indices_atores is None:
            lista_indices_atores = (
                range(len(self.lista_atores)) for _ in range(len(self.lista_atores))
            )

        for ator, influenciador, idx_atores in zip(
            self.lista_atores, lista_inluenciador, lista_indices_atores
        ):
            circulo_ator = [
                _ator for i, _ator in enumerate(self.lista_atores) if i in idx_atores
            ]
            influenciadores = influenciador.listar_influenciadores(ator, circulo_ator)
            ator.set_influenciadores(influenciadores)

    def emitir_opiniao(self) -> Ator:
        ator = self.sorteia_ator()
        influenciador = ator.sorteia_influenciador()
        ator.set_opiniao(self.tempo, influenciador, influenciador.get_opiniao())

        self.log.update({self.tempo: (ator, influenciador)})

        self.tempo += 1

        return ator

    def simular_modelo(self, instante: int) -> list[Any]:
        for _ in range(instante):
            self.emitir_opiniao()

        return self.get_opinioes()

    def sorteia_ator(self) -> Ator:
        return random.choice(self.lista_atores)

    def get_opinioes(self) -> Sequence:
        return [ator.get_opiniao() for ator in self.lista_atores]

    def is_opiniao_unanime(self) -> bool:
        opinioes = iter(self.get_opinioes())
        primeira_opiniao = next(opinioes)
        return all(opiniao == primeira_opiniao for opiniao in opinioes)

    def get_origem(self, ator: Ator, tempo: int | None = None) -> Ator:
        if tempo is None:
            tempo = self.tempo
        elif tempo == 0:
            return ator
        instantes = ator.log.keys()
        origem_opiniao = max(filter(lambda x: x <= tempo, instantes))

        influencia = ator.log[origem_opiniao][0]
        return self.get_origem(influencia, origem_opiniao)
