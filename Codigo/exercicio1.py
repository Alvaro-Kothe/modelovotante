import random
from typing import Counter

import ModeloVotante
from ModeloVotante.influenciador import InfluenciadorRede


def gera_atores_1():
    return [ModeloVotante.Ator(i + 1, 1 if (i + 1) % 2 == 0 else -1) for i in range(10)]


def ex_1_2():

    modelo = ModeloVotante.ModeloVotante(gera_atores_1(), InfluenciadorRede())

    return modelo.simular_modelo(200)


def ex_1_5():
    vetor_1_eq_6 = []
    for _ in range(100):
        modelo = ModeloVotante.ModeloVotante(gera_atores_1(), InfluenciadorRede())

        modelo.simular_modelo(200)
        ator_origem = modelo.get_origem(modelo.lista_atores[0], 200)

        vetor_1_eq_6.append(
            ator_origem == modelo.get_origem(modelo.lista_atores[5], 200)
        )

    return sum(vetor_1_eq_6) / len(vetor_1_eq_6)


if __name__ == "__main__":
    random.seed(2)
    print(ex_1_2())

    random.seed(1)
    # Ex 1.3
    counter = Counter(str(ex_1_2()) for _ in range(30))
    for k, v in counter.most_common(4):
        print(k, v, "Ocorrências")

    random.seed(1)
    print(ex_1_5())
